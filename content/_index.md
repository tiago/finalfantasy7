O QUE É ?

Final Fantasy VII é um jogo eletrônico de RPG desenvolvido e publicado pela SquareSoft . É o sétimo título principal da série Final Fantasy e foi lançado originalmente para PlayStation em 1997 e depois também para Microsoft Windows no ano seguinte. A história segue Cloud Strife, um mercenário que junta-se a uma organização ecoterrorista a fim de impedir que megacorporação Shinra use a essência vital do planeta como uma fonte de energia. Acontecimentos colocam Cloud e seus aliados atrás de Sephiroth, um “super-humano” que deseja destruir o planeta. A jogabilidade tem navegação e exploração de diversos ambientes e sistema de batalha baseado em turnos.

O MUNDO 

O mundo de jogo é semelhante ao daquele de Final Fantasy VI, em que é muito mais avançado tecnologicamente do que nos cinco primeiros jogos da série. De forma geral, a tecnologia e a sociedade são semelhantes ao meio social de uma ficção científica industrial ou pós-industrial. O mundo de Final Fantasy VII é chamado dentro de jogo de simplesmente “o Planeta”, porém foi retroativamente nomeado Gaia e é formado por três massas continentais principais. O continente do leste é onde fica a cidade de Midgar, uma metrópole industrial que serve como capital e abriga a sede da Shinra Electric Power Company, uma poderosa empresa de energia que chega a operar como o verdadeiro governo do mundo. Outros lugares no continente do leste incluem Junon, uma base militar da Shinra; Fort Kondor, uma fortificação que esconde um reator Mako; um rancho chocobo, onde diferentes tipos de animais podem ser criados; e Kalm, um vilarejo perto de Midgar.

DESENVOLVIMENTO

Os planejamentos iniciais de Final Fantasy VII começaram em 1994, logo depois do lançamento de Final Fantasy VI. Na época, a intenção era fazer um jogo com gráficos bidimensionais para o Super Nintendo Entertainment System. Hironobu Sakaguchi, criador da série Final Fantasy, originalmente planejou uma história para o jogo, porém esse primeiro roteiro era completamente diferente do produto final. Tetsuya Nomura, desenhista de personagens, disse que Sakaguchi “queria fazer algo parecido com uma história de detetive”. A primeira parte envolvia um personagem “temperamental” chamado de Detetive Joe perseguindo os personagens principais após estes terem explodido a cidade de Midgar, com conceitos como a Corrente da Vida já estando presentes nessa primeira versão. A história mudou com a chegada de Nomura e dos roteiristas Yoshinori Kitase e Kazushige Nojima, com estes dois últimos introduzindo a Shinra e a AVALANCHE, além da história de Cloud Strife e sua relação com Sephiroth. Masato Kato foi mais tarde também trazido para o projeto e contribuiu escrevendo três cenas para o jogo.

PERSONAGENS

    Cloud Strife
    Barret Wallace
    Tifa Lockhart
    Aerith Gainsborough
    Red XIII
    Cait Sith
    Cricid Highwind
    Yuffie Kisaragi
    Vincent Valentine
    Sephiroth

Retirado e adaptado de https://pt.wikipedia.org/wiki/Final_Fantasy_VII
